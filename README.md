# Word2Vec

## Requires

This project is written in Python 3.

Please install Python3.

## Create your work environment

### Anaconda

Anaconda regroups some tools to work with Python.
- conda
- python
- jupyter

Download Anaconda <a href="https://www.anaconda.com/download">here</a>

Conda is a package manager to manage virtual environment and install packages.

Some conda commands : 

#### Upgrade conda

    conda upgrade conda

#### Create environment

    conda create -n [env-name]

#### Activate the environment you created

    soruce activate [env-name]

#### Take a look at the environment you created

    conda info

#### Install a package

    conda install [package-name]

#### Delete environment

    conda env remove --name [env-name]

### Numpy

Please install numpy : 

    conda install numpy

### Now, we'll have to install TensorFlow & Keras

First, install pip3 in your environment.

    conda install pip3

Then, install tensorFlow.

    pip3 install --upgrade tensorflow

Finally, run this following command :

    pip3 install Keras

## Launch

In your terminal, please run :

    jupyter notebook

## Contributors

Violaine HUYNH : vhuynh05@etud.u-pem.fr - M2 Logiciels <br>
Jean-Baptiste PILORGET : jpilorge@etud.u-pem.fr - M2 Logiciels <br>
Florent SIMONNOT : fsimonno@etud.u-pem.fr - M2 Logiciels <br>
Akram MALEK : amalek@etud.u-pem.fr - M2 Logiciels